FROM node:fermium-alpine3.12 as builder
WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .
RUN npm run compile

FROM node:fermium-alpine3.12
WORKDIR /usr/src/app
RUN npm install pm2 -g
COPY package.json ./
ENV NODE_ENV=production
RUN npm install --only=production
COPY ./config ./config
COPY ./static ./static_seed
COPY --from=builder /usr/src/app/lib/ /usr/src/app/lib/
VOLUME [ "/usr/src/app/static" ]
COPY process.yml .
CMD ["pm2-runtime", "process.yml"]
