const fetch = require("node-fetch");
const business = require("./csvjson.json");

Promise.all(
  business.map(async (b) => {
    return await fetch("http://localhost:3030/users", {
      method: "POST",
      body: JSON.stringify({
        email: b.email,
        password: b.email,
        rol: "business",
        businessName: b.businessName,
        businessCategory: b.businessCategory,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
  })
).then((result) => console.log(result));
