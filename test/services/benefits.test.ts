import assert from "assert";
import app from "../../src/app";
import * as sinon from "sinon";
import { user } from "../../src/models/users.model";
import { benefit } from "../../src/models/benefits.model";

describe("'benefits' service", () => {
  it("registered the service", () => {
    const service = app.service("benefits");

    assert.ok(service, "Registered the service");
  });
  describe("'benefits' permissions", () => {
    let clock: sinon.SinonFakeTimers;
    let benefit: benefit;
    let businessUser: user, userUser: user;
    const businessUserData = {
      email: "businessuser@mail.com",
      password: "business",
      rol: "business",
    };
    const userUserData = {
      email: "useruser@mail.com",
      password: "user",
      rol: "user",
    };
    beforeEach(() => {
      clock = sinon.useFakeTimers();
    });
    before(async () => {
      businessUser = await app.service("users").create({
        ...businessUserData,
      });
      userUser = await app.service("users").create({
        ...userUserData,
      });
    });
    afterEach(() => {
      clock.restore();
    });
    after(async () => {
      businessUser = await app.service("users").remove(businessUser._id);
      userUser = await app.service("users").remove(userUser._id);
    });

    it("allows business to create benefits", async () => {
      benefit = await app.service("benefits").create(
        {
          name: "combo loco",
          type: "P",
          description: "un combo re loco",
        },
        {
          user: businessUser,
        }
      );
    });

    it(`doesn't allow business to patch benefit below ${app.get(
      "benefitChangeFrequency"
    )} days old`, async () => {
      await clock.tickAsync(1000 * 60 * 60 * 12);
      try {
        await app.service("benefits").patch(
          benefit._id,
          {
            name: "combo loco 2",
          },
          {
            user: businessUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }
    });
    it(`allows business to patch benefit over ${app.get(
      "benefitChangeFrequency"
    )} days old`, async () => {
      await clock.tickAsync(
        1000 * 60 * 60 * (24 * app.get("benefitChangeFrequency") + 1)
      );

      const patchedBenefit = await app.service("benefits").patch(
        benefit._id,
        {
          name: "combo loco 2",
        },
        {
          user: businessUser,
        }
      );
      assert(patchedBenefit.name !== benefit.name);
      assert.strictEqual(patchedBenefit.name, "combo loco 2");
    });
    it("doesn't allow users to create benefits", async () => {
      try {
        await app.service("benefits").create(
          {
            name: "combo loco",
            type: "P",
            description: "un combo re loco",
          },
          {
            user: userUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }
    });
  });
});
