import assert from "assert";
import app from "../../src/app";
import { countToId } from "../../src/hooks/code";

describe("'codes' service", () => {
  let testBenefit: any;

  before(async () => {
    const user = await app.service("users").create({
      email: "codetestsbenefituser@mail.com",
      password: "admin",
      rol: "business",
    });
    const params = { user };

    testBenefit = await app.service("benefits").create(
      {
        name: "combo loco",
        type: "P",
        description: "un combo re loco",
      },
      params
    );
  });
  it("registered the service", () => {
    const service = app.service("codes");

    assert.ok(service, "Registered the service");
  });
  it("should create codes secuencially", async () => {
    const user = await app.service("users").create({
      email: "codetestsuser@mail.com",
      password: "user",
      rol: "user",
    });
    const params = { user };

    const code_1 = await app
      .service("codes")
      .create({ benefit: testBenefit._id, used: false }, params);
    const code_2 = await app
      .service("codes")
      .create({ benefit: testBenefit._id, used: false }, params);
    assert.strictEqual(Number(code_2.code) - Number(code_1.code), 1);
  });
  it("should generate right IDs even on overflow", () => {
    assert.strictEqual(countToId(1), "0001");
    assert.strictEqual(countToId(0), "0000");
    assert.strictEqual(countToId(35), "000z");
    assert.strictEqual(countToId(36), "0010");
    assert.strictEqual(countToId(36 * 36 * 36 * 36 - 1), "0000");
    assert.strictEqual(countToId(36 * 36 * 36 * 36), "0001");
    assert.strictEqual(countToId(36 * 36 * 36 * 36 + 1), "0002");
  });
});
