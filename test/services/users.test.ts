import assert from "assert";
import app from "../../src/app";
import { user } from "../../src/models/users.model";

describe("'users' service", () => {
  let user: any;
  afterEach(async () => {
    try {
      await app.service("users").remove(user._id);
    } catch (error) {}
  });
  it("registered the service", () => {
    const service = app.service("users");

    assert.ok(service, "Registered the service");
  });
  // describe("'users' roles", () => {

  it("allows to create users with rol: user", async () => {
    const userData = {
      email: "hasroleuser@mail.com",
      password: "pwd",
      rol: "user",
    };
    user = await app.service("users").create(userData);
  });
  it("fails to create users with taken email", async () => {
    const userData = {
      email: "hasroleuser@mail.com",
      password: "pwd",
      rol: "user",
    };
    user = await app.service("users").create(userData);
    try {
      await app.service("users").create(userData);
    } catch (error) {
      assert.strictEqual(error.type, "FeathersError");
      assert.strictEqual(error.code, 409);
      assert.strictEqual(error.className, "conflict");
    }
  });
  it("allows to create users with rol: business", async () => {
    const userData = {
      email: "hasrolebusiness@mail.com",
      password: "pwd",
      rol: "business",
    };
    user = await app.service("users").create(userData);
  });
  it("fails to create users with rol: admin", async () => {
    const userData = {
      email: "hasroleadmin@mail.com",
      password: "pwd",
      rol: "admin",
    };
    try {
      await app.service("users").create(userData);
    } catch (error) {
      assert.strictEqual(error.type, "FeathersError");
      assert.strictEqual(error.code, 403);
      assert.strictEqual(error.className, "forbidden");
    }
  });
  it("fails to create users with rol: business-admin", async () => {
    const userData = {
      email: "hasrolebusinessadmin@mail.com",
      password: "pwd",
      rol: "business-admin",
    };
    try {
      await app.service("users").create(userData);
    } catch (error) {
      assert.strictEqual(error.type, "FeathersError");
      assert.strictEqual(error.code, 403);
      assert.strictEqual(error.className, "forbidden");
    }
  });
  it("fails to create users with rol: user-admin", async () => {
    const userData = {
      email: "hasroleauserdmin@mail.com",
      password: "pwd",
      rol: "user-admin",
    };
    try {
      await app.service("users").create(userData);
    } catch (error) {
      assert.strictEqual(error.type, "FeathersError");
      assert.strictEqual(error.code, 403);
      assert.strictEqual(error.className, "forbidden");
    }
  });

  describe("'users' permissions", () => {
    let user_2: any, user: any;
    const userData1 = {
      email: "hasroleuser1@mail.com",
      password: "pwd",
      rol: "user",
    };
    const userData2 = {
      email: "hasroleuser2@mail.com",
      password: "pwd",
      rol: "user",
    };
    before(async () => {
      user = await app.service("users").create(userData1);
      user_2 = await app.service("users").create(userData2);
    });

    it("allows user to patch himself", async () => {
      await app
        .service("users")
        .patch(user._id, { password: "1234" }, { user });
    });
    it("doesn't allow user to patch his role", async () => {
      let unpatchedUser = await app
        .service("users")
        .patch(user._id, { role: "admin" }, { user });
      assert.strictEqual(unpatchedUser.rol, "user");
      unpatchedUser = await app
        .service("users")
        .patch(user._id, { role: "user-admin" }, { user });
      assert.strictEqual(unpatchedUser.rol, "user");
      unpatchedUser = await app
        .service("users")
        .patch(user._id, { role: "business-admin" }, { user });
      assert.strictEqual(unpatchedUser.rol, "user");
      unpatchedUser = await app
        .service("users")
        .patch(user._id, { role: "non-existent-rol" }, { user });
      assert.strictEqual(unpatchedUser.rol, "user");
    });
    it("doesn't allow user to patch his email", async () => {
      const unpatchedUser = await app
        .service("users")
        .patch(user._id, { email: "hasroleuser0@gmail.com" }, { user });
      assert.strictEqual(unpatchedUser.email, userData1.email);
    });
    it("fails to patch other than himself", async () => {
      try {
        await app
          .service("users")
          .patch(user_2._id, { password: "1234" }, { user });
      } catch (error) {
        assert.strictEqual(error.type, "FeathersError");
        assert.strictEqual(error.code, 404);
        assert.strictEqual(error.className, "not-found");
      }

      const unpatchedUser = await app.service("users").get(user_2._id);
      assert.strictEqual(unpatchedUser.password, user_2.password);
    });
    it("fails to delete other than himself", async () => {
      try {
        await app.service("users").remove(user_2._id, { user });
      } catch (error) {
        assert.strictEqual(error.type, "FeathersError");
        assert.strictEqual(error.code, 404);
        assert.strictEqual(error.className, "not-found");
      }

      const unpatchedUser = await app.service("users").get(user_2._id);
      assert.strictEqual(unpatchedUser.password, user_2.password);
    });
    it("allows admin role to create admins", async () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { user } = await app.service("authentication").create(
        {
          strategy: "local",
          email: "admin@mail.com",
          password: "admin",
        },
        {}
      );

      let newAdminUser: user = await app.service("users").create(
        { email: "admin2@gmail.com", password: "password", rol: "admin" },
        {
          user,
        }
      );
      assert.strictEqual(newAdminUser.rol, "admin");

      newAdminUser = await app.service("users").create(
        {
          email: "useradmin@gmail.com",
          password: "password",
          rol: "user-admin",
        },
        {
          user,
        }
      );
      assert.strictEqual(newAdminUser.rol, "user-admin");
      try {
        await app.service("users").create(
          {
            email: "notaroluser@mail.com",
            password: "password",
            rol: "admin",
          },
          {
            newAdminUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }

      try {
        await app.service("users").create(
          {
            email: "notaroluser@mail.com",
            password: "password",
            rol: "user-admin",
          },
          {
            newAdminUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }
      try {
        await app.service("users").create(
          {
            email: "notaroluser@mail.com",
            password: "password",
            rol: "user-admin",
          },
          {
            newAdminUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }

      newAdminUser = await app.service("users").create(
        {
          email: "businessadmin@gmail.com",
          password: "password",
          rol: "business-admin",
        },
        {
          user,
        }
      );
      assert.strictEqual(newAdminUser.rol, "business-admin");
      try {
        await app.service("users").create(
          {
            email: "notaroluser@mail.com",
            password: "password",
            rol: "admin",
          },
          {
            newAdminUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }
      try {
        await app.service("users").create(
          {
            email: "notaroluser@mail.com",
            password: "password",
            rol: "business-admin",
          },
          {
            newAdminUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }
      try {
        await app.service("users").create(
          {
            email: "notaroluser@mail.com",
            password: "password",
            rol: "user-admin",
          },
          {
            newAdminUser,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 403);
        assert.strictEqual(err.className, "forbidden");
      }

      try {
        newAdminUser = await app.service("users").create(
          {
            email: "notaroluser@mail.com",
            password: "password",
            rol: "this-is-not-a-rol",
          },
          {
            user,
          }
        );
      } catch (err) {
        assert.strictEqual(err.type, "FeathersError");
        assert.strictEqual(err.code, 400);
        assert.strictEqual(err.className, "bad-request");
      }
    });
  });
});
