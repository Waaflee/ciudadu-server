import assert from "assert";
import sinon from "sinon";
import app from "../../src/app";
import { benefit } from "../../src/models/benefits.model";
import { user } from "../../src/models/users.model";

describe("'benefits-management' service", () => {
  it("registered the service", () => {
    const service = app.service("benefits-management");

    assert.ok(service, "Registered the service");
  });
  describe("receives notifications on benefit operations", () => {
    let businessUser: user, adminUser: user, benefit: benefit;
    const businessUserData = {
      email: "businessuser@mail.com",
      password: "business",
      rol: "business",
    };
    let clock: sinon.SinonFakeTimers;
    beforeEach(() => {
      clock = sinon.useFakeTimers();
    });
    before(async () => {
      businessUser = await app.service("users").create({
        ...businessUserData,
      });
      {
        const { user } = await app.service("authentication").create(
          {
            strategy: "local",
            email: "businesadmin@mail.com",
            password: "admin",
          },
          {}
        );
        adminUser = user;
      }
    });
    afterEach(() => {
      clock.restore();
    });
    after(async () => {
      businessUser = await app.service("users").remove(businessUser._id);
    });
    it("receives notification on creation", async () => {
      let benefitsNotification = (await app
        .service("benefits-management")
        .find({ user: adminUser })) as any;
      const notificationCount = benefitsNotification.total;
      benefit = await app.service("benefits").create(
        {
          name: "combo loco",
          type: "P",
          description: "un combo re loco",
        },
        {
          user: businessUser,
        }
      );
      benefitsNotification = (await app.service("benefits-management").find({
        user: adminUser,
      })) as any;

      assert.strictEqual(benefitsNotification.total, notificationCount + 1);
      assert.strictEqual(
        benefitsNotification.data[benefitsNotification.data.length - 1]
          .operation,
        "create"
      );
    });
    it("receives notification on patch", async () => {
      let benefitsNotification = (await app
        .service("benefits-management")
        .find({
          user: adminUser,
        })) as any;
      const notificationCount = benefitsNotification.total;
      await clock.tickAsync(
        1000 * 60 * 60 * (24 * app.get("benefitChangeFrequency") + 1)
      );
      await app.service("benefits").patch(
        benefit._id,
        {
          name: "combo loco 2",
        },
        {
          user: businessUser,
        }
      );
      benefitsNotification = (await app.service("benefits-management").find({
        user: adminUser,
      })) as any;
      assert.strictEqual(benefitsNotification.total, notificationCount + 1);
      assert.strictEqual(
        benefitsNotification.data[benefitsNotification.data.length - 1]
          .operation,
        "patch"
      );
    });
    it("receives notification on remove", async () => {
      let benefitsNotification = (await app
        .service("benefits-management")
        .find({
          user: adminUser,
        })) as any;
      const notificationCount = benefitsNotification.total;
      benefit = await app.service("benefits").remove(benefit._id, {
        user: businessUser,
      });
      benefitsNotification = (await app.service("benefits-management").find({
        user: adminUser,
      })) as any;
      assert.strictEqual(benefitsNotification.total, notificationCount + 1);
      assert.strictEqual(
        benefitsNotification.data[benefitsNotification.data.length - 1]
          .operation,
        "remove"
      );
      const deletedData =
        benefitsNotification.data[benefitsNotification.data.length - 1]
          .deletedData;
      assert.strictEqual(deletedData, JSON.stringify(benefit));
    });
  });
});
