import assert from "assert";
import sinon, { SinonFakeTimers } from "sinon";
import app from "../../src/app";
import { checkEntityDate } from "../../src/services/redeems/redeems.class";
// import { code } from "../../src/models/codes.model";
// import { redeem } from "../../src/models/redeems.model";
// import { user } from "../../src/models/users.model";
// import { benefit } from "../../src/models/benefits.model";

describe("'redeems' service", () => {
  let clock: SinonFakeTimers;
  beforeEach(() => {
    clock = sinon.useFakeTimers({ now: new Date("2020-01-03").getTime() });
  });
  afterEach(() => {
    clock.restore();
  });
  it("registered the service", () => {
    const service = app.service("redeems");
    assert.ok(service, "Registered the service");
  });
  it("checks if data is from this month", () => {
    const entity = { createdAt: new Date() };
    assert.strictEqual(checkEntityDate(entity), true);
    clock.tick(1000 * 60 * 60 * 24 * 31);
    assert.strictEqual(checkEntityDate(entity), false);
  });
  // it("should create local entries on predictable times", async () => {
  //   let redeem: redeem = await app.service("redeems").create({});
  //   assert.strictEqual(redeem.simpleDate, "2020-01");
  //   clock.tick(1000 * 60 * 60 * 24 * 31);
  //   redeem = await app.service("redeems").create({});
  //   assert.strictEqual(redeem.simpleDate, "2020-02");
  // });
  // it('should change valued: "used" of code to true on redeem', async () => {
  //   const userUser: user = await app.service("users").create({
  //     email: "redeemtestsuser@mail.com",
  //     password: "user",
  //     rol: "user",
  //   });
  //   const userBusiness: user = await app.service("users").create({
  //     email: "redeemtestsuserbusiness@mail.com",
  //     password: "user",
  //     rol: "business",
  //   });
  //   const benefit: benefit = await app.service("benefits").create(
  //     {
  //       name: "combo loco",
  //       type: "P",
  //       description: "un combo re loco",
  //     },
  //     { user: userBusiness }
  //   );
  //   const code: code = await app
  //     .service("codes")
  //     .create({ benefit: benefit._id }, { user: userUser });
  //   assert.strictEqual(code.used, false);
  //   await app.service("redeems").create({ codeId: code._id });
  //   const redeemCode: code = await app.service("codes").get(code._id);
  //   assert.strictEqual(redeemCode.used, true);
  // });
});
