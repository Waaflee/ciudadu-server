import assert from "assert";
import sinon, { SinonFakeTimers } from "sinon";
import app from "../../src/app";
import { login } from "../../src/models/logins.model";

describe("'logins' service", () => {
  let clock: SinonFakeTimers;
  beforeEach(() => {
    clock = sinon.useFakeTimers({ now: new Date("2020-01-03").getTime() });
  });
  afterEach(() => {
    clock.restore();
  });
  it("registered the service", () => {
    const service = app.service("logins");
    assert.ok(service, "Registered the service");
  });
  it("should create local entries on predictable times", async () => {
    let login = await app.service("logins").create({});
    assert.strictEqual(login.logins, 1);
    assert.strictEqual(login.simpleDate, "2020-01");
    clock.tick(1000 * 60 * 60 * 24 * 31);
    login = await app.service("logins").create({});
    assert.strictEqual(login.logins, 1);
    assert.strictEqual(login.simpleDate, "2020-02");
    clock.tick(1000 * 60 * 60 * 24 * 30);
    login = await app.service("logins").create({});
    assert.strictEqual(login.logins, 1);
    assert.strictEqual(login.simpleDate, "2020-03");
  });
  it("should increment logins on the same month", async () => {
    clock = sinon.useFakeTimers({ now: new Date("2021-01-03").getTime() });
    let login: login = await app.service("logins").create({});
    assert.strictEqual(login.logins, 1);
    assert.strictEqual(login.simpleDate, "2021-01");
    clock.tick(1000 * 60 * 60 * 24 * 1);
    login = await app.service("logins").create({});
    assert.strictEqual(login.logins, 2);
    assert.strictEqual(login.simpleDate, "2021-01");
    clock.tick(1000 * 60 * 60 * 24 * 1);
    login = await app.service("logins").create({});
    assert.strictEqual(login.logins, 3);
    assert.strictEqual(login.simpleDate, "2021-01");
  });

  // it("should increment on user login", async () => {
  //   clock = sinon.useFakeTimers({ now: new Date("2022-01-03").getTime() });
  //   const userData = {
  //     strategy: "local",
  //     email: "logintestsuser@mail.com",
  //     password: "user",
  //     rol: "user",
  //   };

  //   await app.service("users").create(userData);
  //   let login: login = await app.service("logins").create({});
  //   const logins = login.logins;
  //   await app
  //     .service("authentication")
  //     .create({ ...userData }, { provider: "rest" });
  //   login = await app.service("logins").get(login._id);
  //   assert.strictEqual(logins, login.logins + 1);
  //   await app
  //     .service("authentication")
  //     .create({ ...userData }, { provider: "rest" });
  //   login = await app.service("logins").get(login._id);
  //   assert.strictEqual(logins, login.logins + 2);
  // });

  it("shouldn't increment on non user login", async () => {
    clock = sinon.useFakeTimers({ now: new Date("2023-01-03").getTime() });
    const adminData1 = {
      strategy: "local",
      email: "admin@mail.com",
      password: "admin",
    };
    const adminData2 = {
      strategy: "local",
      email: "useradmin@mail.com",
      password: "admin",
    };
    const adminData3 = {
      strategy: "local",
      email: "businesadmin@mail.com",
      password: "admin",
    };
    let login: login = await app.service("logins").create({});
    const logins = login.logins;
    await app
      .service("authentication")
      .create({ ...adminData1 }, { provider: "rest" });
    login = await app.service("logins").get(login._id);
    assert.strictEqual(logins, login.logins);
    await app
      .service("authentication")
      .create({ ...adminData2 }, { provider: "rest" });
    login = await app.service("logins").get(login._id);
    assert.strictEqual(logins, login.logins);
    await app
      .service("authentication")
      .create({ ...adminData3 }, { provider: "rest" });
    login = await app.service("logins").get(login._id);
    assert.strictEqual(logins, login.logins);
  });
});
