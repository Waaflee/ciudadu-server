podman run --rm --name mongodb -p 27017:27017 -v ./mongodb/seed.js:/docker-entrypoint-initdb.d/init-mongo-js:ro -v ./mongodb/data:/data/db -d mongo:4.4.3-bionic 
podman run --rm --name redisdb -p 6379:6379 -d redis:6.0-alpine
# podman cp database.seed.json mongodb:/tmp
# podman exec mongodb sh -c 'exec mongoimport /tmp/database.seed.json -d ciudadu -c users --jsonArray'
# podman exec mongodb sh -c 'exec rm /tmp/database.seed.json'
