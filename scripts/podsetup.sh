podman pod create --name apipod -p 3030:3030 
podman run --rm --pod apipod --name mongodb -d db.ciudadu 
podman run --rm --pod apipod --name redisdb -d docker.io/library/redis:alpine
podman run -it --rm --pod apipod --name server api.ciudadu
docker cp database.seed.json mongodb:/tmp
docker exec mongodb sh -c 'exec mongoimport /tmp/database.seed.json -d ciudadu -c users --jsonArray'
docker exec mongodb sh -c 'exec rm /tmp/database.seed.json'