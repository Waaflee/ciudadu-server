docker stop testingdb > /dev/null 2>&1
docker run --rm --name testingdb -p 27018:27017 -d mongo
docker cp database.seed.json testingdb:/tmp
docker exec testingdb sh -c 'exec mongoimport /tmp/database.seed.json -d ciudadu -c users --jsonArray'
docker exec testingdb sh -c 'exec rm /tmp/database.seed.json'