db.users.insertMany([
  {
    email: "admin@mail.com",
    password: "$2a$10$6pLqIXgyODngR0SJ/hUoRuunWFkOklGDsDiRva2yNtLmVQIL9sYLq",
    rol: "admin",
  },
  {
    email: "useradmin@mail.com",
    password: "$2a$10$6pLqIXgyODngR0SJ/hUoRuunWFkOklGDsDiRva2yNtLmVQIL9sYLq",
    rol: "user-admin",
  },
  {
    email: "businessadmin@mail.com",
    password: "$2a$10$6pLqIXgyODngR0SJ/hUoRuunWFkOklGDsDiRva2yNtLmVQIL9sYLq",
    rol: "business-admin",
  },
]);
