import { Application } from '../declarations';
import users from './users/users.service';
import benefits from './benefits/benefits.service';
import codes from './codes/codes.service';
import redeems from './redeems/redeems.service';
import benefitsManagement from './benefits-management/benefits-management.service';
import logins from './logins/logins.service';
import email from './email/email.service';
import authManagement from './auth-management/auth-management.service';
import notifications from './notifications/notifications.service';
import uploads from './uploads/uploads.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(users);
  app.configure(benefits);
  app.configure(codes);
  app.configure(redeems);
  app.configure(benefitsManagement);
  app.configure(logins);
  app.configure(email);
  app.configure(authManagement);
  app.configure(notifications);
  app.configure(uploads);
}
