import { Params } from "@feathersjs/feathers";
import { Service, MongooseServiceOptions } from "feathers-mongoose";
import { Application } from "../../declarations";
import { login } from "../../models/logins.model";
import { Data, toMonthAndYear } from "../redeems/redeems.class";

interface RedeemsQueryResult {
  total: number;
  limit: number;
  skip: number;
  data: [login];
}

type loginData = login & Partial<Data>;

export class Logins extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<MongooseServiceOptions>, app: Application) {
    super(options);
  }
  async create(data: Partial<login>, params?: Params): Promise<any> {
    const latest = await this.findLatest();
    if (latest) {
      // At least 1 local counter exists
      // if (checkEntityDate(latest as Data)) {
      if (latest.simpleDate === toMonthAndYear(new Date())) {
        // Login is being rcreated at same month as latest local entry
        // Update current month local counter to used++
        return super.patch(latest._id, { logins: latest.logins + 1 }, params);
      } else {
        // Login is being created
        // Update current month local counter to login++
        return super.create(
          { logins: 1, simpleDate: toMonthAndYear(new Date()) },
          params
        );
      }
    } else {
      // No previous entry exists, creating a new one
      return super.create(
        { logins: 1, simpleDate: toMonthAndYear(new Date()) },
        params
      );
    }
  }
  // Finds the latest local entry
  async findLatest(): Promise<loginData | undefined> {
    const data: unknown = await super.find({
      query: {
        $limit: 1,
        $sort: {
          createdAt: -1,
        },
      },
    });
    return (data as RedeemsQueryResult)[0];
  }
}
