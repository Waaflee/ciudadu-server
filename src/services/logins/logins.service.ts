// Initializes the `logins` service on path `/logins`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Logins } from './logins.class';
import createModel from '../../models/logins.model';
import hooks from './logins.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'logins': Logins & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/logins', new Logins(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('logins');

  service.hooks(hooks);
}
