import * as authentication from "@feathersjs/authentication";
import { disallow, iff } from "feathers-hooks-common";
import { HookContext } from "../../declarations";
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;
const isAction = (...args: string[]) => (context: HookContext): boolean =>
  args.includes(context.data.action);

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [
      iff(isAction("passwordChange", "identityChange"), authenticate("jwt")),
    ],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
