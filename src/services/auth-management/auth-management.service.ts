// Initializes the `auth-management` service on path `/auth-management`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations";
import notifier from "./notifier";
import hooks from "./auth-management.hooks";
import authManagement from "feathers-authentication-management-ts";

// Add this service to the service type index
declare module "../../declarations" {
  interface ServiceTypes {
    authManagement: ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  // Initialize our service with any options it requires
  // const TOKEN_DURATION = 1000 * 60 * 60 * app.get("purgeUsersInterval");
  app.configure(authManagement(notifier(app)));
  // app.configure(
  //   authManagement({
  //     notifier: notifier(app),
  //     delay: TOKEN_DURATION,
  //   })
  // );

  // Get our initialized service so that we can register hooks
  const service = app.service("authManagement");

  service.hooks(hooks);
}
