import { Id, Params } from "@feathersjs/feathers";
import { Service, MongooseServiceOptions } from "feathers-mongoose";
import { Application } from "../../declarations";

export interface Data {
  _id: Id;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}

interface RedeemsData extends Data {
  type: string;
  created: number;
  used: number;
}
export interface RedeemsResponse extends RedeemsData {
  globalCounter: RedeemsData;
}

interface RedeemsQueryResult {
  total: number;
  limit: number;
  skip: number;
  data: [RedeemsData];
}

interface createData {
  codeId?: string | Id;
}

// Converts date to YYYY-MM format.
export const toMonthAndYear = (date: Date): string => {
  return [
    date.getFullYear().toString(),
    (date.getMonth() + 1).toString().padStart(2, "0"),
  ].join("-");
};

// Checks if date is from this current month
export const isFromThisMonth = (latestDate: Date): boolean => {
  const currentDate: Date = new Date();
  const currentMonthAndYear: string = toMonthAndYear(currentDate);
  const latestMonthAndYear: string = toMonthAndYear(latestDate);
  return currentMonthAndYear === latestMonthAndYear ? true : false;
};

export const checkEntityDate = (entity: Partial<Data>): boolean => {
  return isFromThisMonth(entity.createdAt);
};

export class Redeems extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<MongooseServiceOptions>, app: Application) {
    super(options);
  }
  async create(data: createData, params?: Params): Promise<any> {
    const globalCounter = await this.findGlobalCounter();
    const latest = await this.findLatest();
    if (globalCounter && latest) {
      // Global couner and at least 1 local counter exists
      if (checkEntityDate(latest as RedeemsData)) {
        // Code is being redeemed/created at same month as latest local entry
        if (data.codeId) {
          // Code is being redeemed
          // Update global and current month local counter to used++
          super.patch(globalCounter._id, {
            used: globalCounter.used + 1,
          });
          return super.patch(latest._id, { used: latest.used + 1 }, params);
        } else {
          // Code is being created
          // Update global and current month local counter to creatd++
          super.patch(globalCounter._id, {
            created: globalCounter.created + 1,
          });
          return super.patch(
            latest._id,
            { created: latest.created + 1 },
            params
          );
        }
      } else {
        // Code is not being redeemed/created at same month as latest local entry
        // Therefore must be a new month and a new local entry is created
        if (data.codeId) {
          // Code is being redeemed so the global counter and a new local entry
          // updated to used++
          super.patch(globalCounter._id, {
            used: globalCounter.used + 1,
          });
          return super.create(
            { used: latest.used + 1, simpleDate: toMonthAndYear(new Date()) },
            params
          );
        } else {
          // Code is being created so the global counter and a new local entry
          // updated to created++
          super.patch(globalCounter._id, {
            created: globalCounter.created + 1,
          });
          return super.create(
            {
              created: latest.created + 1,
              simpleDate: toMonthAndYear(new Date()),
            },
            params
          );
        }
      }
    } else {
      // No previous data available: Create global counter and local month counter
      // And set ther created field to 1
      super.create({ type: "global", created: 1, used: 0 }, params);
      return super.create(
        {
          type: "local",
          created: 1,
          used: 0,
          simpleDate: toMonthAndYear(new Date()),
        },
        params
      );
    }
  }
  // Finds the latest local entry
  async findLatest(): Promise<RedeemsData | undefined> {
    const data: unknown = await super.find({
      query: {
        type: "local",
        $limit: 1,
        $sort: {
          createdAt: -1,
        },
      },
    });
    return data[0] as RedeemsData;
  }
  // Finds the global counter (similar to findOne)
  async findGlobalCounter(): Promise<RedeemsData | undefined> {
    const data: unknown = await super.find({
      query: {
        type: "global",
        $limit: 1,
      },
    });
    return data[0] as RedeemsData;
  }
}
