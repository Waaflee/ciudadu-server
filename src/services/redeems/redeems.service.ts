// Initializes the `redeems` service on path `/redeems`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations";
import { Redeems } from "./redeems.class";
import createModel from "../../models/redeems.model";
import hooks from "./redeems.hooks";

// Add this service to the service type index
declare module "../../declarations" {
  interface ServiceTypes {
    redeems: Redeems & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
  };

  // Initialize our service with any options it requires
  app.use("/redeems", new Redeems(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("redeems");

  service.hooks(hooks);
}
