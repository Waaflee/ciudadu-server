import * as authentication from "@feathersjs/authentication";
import { disallow, iff } from "feathers-hooks-common";
import { HookContext } from "../../declarations";
import app from "../../app";
import { Conflict, NotFound } from "@feathersjs/errors";
import checkPermissions from "feathers-permissions";
import { roles } from "../../models/users.model";
import { verifyHooks } from "../../hooks";
import { allowConfirmed } from "../users/users.hooks";
// Don't remove this comment. It's needed to format import lines nicely.
const { authenticate } = authentication.hooks;

const addGlobalCount = async (context: HookContext): Promise<HookContext> => {
  context.result.globalCounter = await app
    .service("redeems")
    .findGlobalCounter();
  return context;
};

const processCodeRedeem = async (code: any) => {
  const benefit = await app.service("benefits").get(code.benefit);
  await app.service("codes").patch(code._id, { used: true });
  await app.service("benefits").patch(benefit._id, { uses: benefit.uses + 1 });
};

const hasCodeIdPayload = (context: HookContext) =>
  context.data.codeId && context.data.codeId !== "";

const redeemCode = async (context: HookContext): Promise<HookContext> => {
  let code: any;
  try {
    code = await app.service("codes").get(context.data.codeId, context.params);
  } catch (error) {
    throw new NotFound("El código seleccionado no existe");
  }
  if (code.used) {
    throw new Conflict("El código seleccionado ya ha sido usado");
  }
  processCodeRedeem(code);
  return context;
};

export default {
  before: {
    all: [authenticate("jwt")],
    find: [
      checkPermissions({
        roles: [roles.userAdmin],
        field: "rol",
        error: true,
      }),
    ],
    get: [
      checkPermissions({
        roles: [roles.userAdmin],
        field: "rol",
        error: true,
      }),
    ],
    create: [
      iff(
        hasCodeIdPayload,
        checkPermissions({
          roles: [roles.business],
          field: "rol",
          error: true,
        }),
        verifyHooks.isVerified(),
        allowConfirmed,
        redeemCode
      ).else(disallow("external")),
    ],
    update: [disallow()],
    patch: [disallow("external")],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [addGlobalCount],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
