import * as feathersAuthentication from "@feathersjs/authentication";
import * as local from "@feathersjs/authentication-local";
import { setField } from "feathers-authentication-hooks";
import {
  disallow,
  iff,
  isProvider,
  preventChanges,
} from "feathers-hooks-common";
import {
  verifyHooks,
  isRequestedRolAdmin,
  sendVerificationEmail,
} from "../../hooks";

import { roles } from "../../models/users.model";
import checkPermissions from "feathers-permissions";
import { HookContext } from "../../app";
import {
  redisBeforeHook as redisBefore,
  redisAfterHook as redisAfter,
  hookCache as cache,
} from "feathers-hooks-rediscache";
import { Forbidden } from "@feathersjs/errors";
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;

const limitToUser = setField({
  from: "params.user._id",
  as: "params.query._id",
});

const isUser = (context: HookContext): boolean =>
  context.params.user.rol === roles.user ||
  context.params.user.rol === roles.business;

const protectedFields = [
  "password",
  "updatedAt",
  "createdAt",
  "verifyToken",
  "verifyShortToken",
  "verifyExpires",
  "resetToken",
  "resetExpires",
  "verifyChanges",
  "__v",
];

const isRequestingBusiness = (context: HookContext): boolean =>
  context.params.query?.rol === roles.business;

const isConfirmed = (context: HookContext): boolean =>
  context.params.user?.isConfirmed;

const changingConfirmation = (context: HookContext): boolean =>
  context.data.isConfirmed;

export const allowConfirmed = (context: HookContext): HookContext => {
  if (!isConfirmed(context)) {
    throw new Forbidden("You are not authorized to do this.");
  } else {
    return context;
  }
};
// Only admins can see admins and only 'admin' can create admins.
export default {
  before: {
    all: [],
    find: [
      iff(
        isProvider("external"),
        // iff(isRequestingBusiness, redisBefore()).else(
        authenticate("jwt")
        // checkPermissions({
        //   roles: [roles.admin, roles.userAdmin, roles.businessAdmin],
        //   field: "rol",
        //   error: true,
        // })
        // )
      ),
    ],
    get: [authenticate("jwt")],
    create: [
      // after user is created, add verification fields to user record
      verifyHooks.addVerification(),
      hashPassword("password"),
      iff(
        isRequestedRolAdmin,
        authenticate("jwt"),
        checkPermissions({
          roles: [roles.admin],
          field: "rol",
          error: true,
        })
      ),
    ],
    update: [disallow("external")],
    patch: [
      authenticate("jwt"),
      iff(
        isProvider("external"),
        checkPermissions({
          roles: [
            roles.admin,
            roles.user,
            roles.userAdmin,
            roles.business,
            roles.businessAdmin,
          ],
          field: "rol",
          error: true,
        }),
        iff(
          isUser,
          verifyHooks.isVerified(),
          iff(
            isConfirmed,
            preventChanges(
              true,
              "name",
              "lastName",
              "birthday",
              "dni",
              "university",
              "facultyAndCareer",
              "registration"
            )
          ),
          limitToUser
        ),
        iff(
          changingConfirmation,
          checkPermissions({
            roles: [roles.userAdmin],
            field: "rol",
            error: true,
          })
        ),
        preventChanges(
          false,
          "email",
          "rol",
          "isVerified",
          "verifyToken",
          "verifyShortToken",
          "verifyExpires",
          "verifyChanges",
          "resetToken",
          "resetShortToken",
          "resetExpires"
        )
      ),
      hashPassword("password"),
    ],
    remove: [
      authenticate("jwt"),
      iff(isProvider("external"), verifyHooks.isVerified(), limitToUser),
    ],
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      // prevent leak of these user information fields
      protect("password"),
    ],
    find: [
      // iff(
      //   isProvider("external"),
      //   protect(...protectedFields),
      //   cache({ duration: 3600 * 24 * 7 }),
      //   redisAfter()
      // ),
    ],
    get: [protect(...protectedFields)],
    create: [
      sendVerificationEmail,
      verifyHooks.removeVerification(),
      protect(...protectedFields),
    ],
    update: [protect(...protectedFields)],
    patch: [protect(...protectedFields)],
    remove: [protect(...protectedFields)],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
