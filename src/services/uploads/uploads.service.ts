// Initializes the `uploads` service on path `/uploads`
import { Params, ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations";
// import { Uploads } from './uploads.class';
import hooks from "./uploads.hooks";
import { fileFilter as filters, limits } from "./uploads.filters";
import multer from "multer";
import blobService from "feathers-blob";
import fs from "fs-blob-store";

// Add this service to the service type index
declare module "../../declarations" {
  interface ServiceTypes {
    // 'uploads': Uploads & ServiceAddons<any>;
    uploads: {
      create(data: any, params?: Params);
      get(id: string, params?: Params);
      remove(id: string, params?: Params);
      // filters: any;
    } & ServiceAddons<any>;
  }
}

export const storage = multer.diskStorage({
  // destination: function (req, file, cb) {
  //   cb(null, "/tmp/my-uploads");
  // },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now());
  },
});

const multipartMiddleware = multer({
  // storage: storage,
  fileFilter: filters,
  limits: limits,
});

// File storage location. Folder must be created before upload.
// Example: './uploads' will be located under feathers app top level.
const blobStorage = fs("./static");

export default function (app: Application): void {
  // Initialize our service with any options it requires
  // app.use('/uploads', new Uploads(options, app));
  app.use(
    "/uploads",

    // multer parses the file named 'uri'.
    // Without extra params the data is
    // temporarily kept in memory
    multipartMiddleware.single("uri"),

    // transfer the received file to feathers
    function (req: any, res, next) {
      req.feathers.file = req.file;
      next();
    },

    blobService({ Model: blobStorage })
  );

  // Get our initialized service so that we can register hooks
  const service = app.service("uploads");

  service.hooks(hooks);

  // if (service.filter) {
  //   service.filter(filters);
  // }
}
