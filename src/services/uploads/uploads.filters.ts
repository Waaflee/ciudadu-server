import { BadRequest } from "@feathersjs/errors";

interface file {
  fieldname: any;
  originalname: any;
  encoding: any;
  mimetype: string;
  size: any;
  destination: any;
  filename: any;
  path: any;
  buffer: any;
}

export const fileFilter = (
  req: Request & any,
  file: Partial<file>,
  cb: (err: Error, success: boolean) => void
): void => {
  if (
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "application/pdf"
  ) {
    cb(null, true);
  } else {
    cb(
      new BadRequest(
        "file type is not supported (must be of type: jpg, jpeg, png or pdf)"
      ),
      false
    );
  }
};

export const limits = {
  fieldSize: 1e6, // Max field value size in bytes, here it's 1MB
  fileSize: 1e6, //  The max file size in bytes, here it's 1MB
  files: 1,
  // READ MORE https://www.npmjs.com/package/multer#limits
};
