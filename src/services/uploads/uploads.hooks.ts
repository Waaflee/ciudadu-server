import * as authentication from "@feathersjs/authentication";
import {
  disallow,
  discard,
  iff,
  isNot,
  isProvider,
} from "feathers-hooks-common";
import dauria from "dauria";
import { HookContext } from "../../declarations";
import { BadRequest, Forbidden } from "@feathersjs/errors";
import checkPermissions from "feathers-permissions";
import { roles, user } from "../../models/users.model";
import { isAdmin, verifyHooks } from "../../hooks";
import { allowConfirmed } from "../users/users.hooks";
import { benefit } from "../../models/benefits.model";
import mongoose from "mongoose";
// import sharp from "sharp";
import logger from "../../logger";
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

const toDataURI = (context: HookContext): HookContext => {
  if (!context.data.uri && context.params.file) {
    const file = context.params.file;
    // if (
    //   file.mimetype.startsWith("image") &&
    //   context.params.query.type === "profile"
    // ) {
    //   try {
    //     sharp(file.buffer)
    //       .resize(250, 250)
    //       .toFormat("jpeg")
    //       .jpeg({ quality: 60 })
    //       .toBuffer((err, buffer) => {
    //         file.buffer = buffer;
    //         if (err) {
    //           logger.error("couldn't process file");
    //         } else {
    //           logger.info("profile picture successfully processed");
    //         }
    //         context.params.extension = "jpg";
    //       });
    //   } catch (error) {
    //     logger.error("couldn't process file");
    //   }
    // } else if (file.mimetype.startsWith("application")) {
    //   context.params.extension = "pdf";
    // }

    const uri = dauria.getBase64DataURI(file.buffer, file.mimetype);
    context.data = { uri: uri };
  }
  return context;
};

const setFileName = (context: HookContext): HookContext => {
  let filePath = "";
  let storedFilePath = "";
  switch (context.params.query.type) {
    case "document":
      filePath = "/private/uploads/documents/";
      storedFilePath = "/documents/";
      break;
    case "profile":
      filePath = "/public/uploads/pictures/profile/";
      storedFilePath = "/pictures/profile/";
      break;
    case "business":
      filePath = "/public/uploads/pictures/business/";
      storedFilePath = "/pictures/business/";
      break;
    case "benefit":
      filePath = "/public/uploads/pictures/benefits/";
      storedFilePath = "/pictures/benefits/";
      break;
    default:
      break;
  }
  const fileName = `${context.params.user.email}-${Date.now()}-${
    context.params.user._id
  }-${context.params.file.originalname}`;
  context.data.storedFilePath = storedFilePath + fileName;
  context.data.id = filePath + fileName;
  // context.data.id =
  //   filePath + `${Date.now()}-${context.params.file.originalname}`;

  return context;
};

const setField = (context: HookContext): HookContext => {
  let field: string;
  switch (context.params.query.type) {
    case "document":
      field = "certificate";
      break;
    case "profile":
      field = "profilePicture";
      break;
    case "business":
      field = "businessPicture";
      break;
    case "benefit":
      break;
    default:
      break;
  }
  context.data.field = field;
  return context;
};

const purgeOrphanFiles = async (context: HookContext): Promise<HookContext> => {
  const { field } = context.data;
  if (field && field !== "") {
    const user: user = await context.app
      .service("users")
      .get(context.params.user._id);
    if (
      !["/private/default.pdf", "/pictures/default.jpg"].includes(user[field])
    ) {
      try {
        await context.app.service("uploads").remove(user[field]);
      } catch (error) {
        // logger.error(error);
        console.log(error);
      }
    }
  } else if (context.params.query.type === "benefit") {
    const benefit: benefit = await context.app
      .service("benefits")
      .get(context.params.query.benefit);
    if (benefit.picture !== "/pictures/benefits/default.jpg") {
      const params = context.params;
      params.provider = undefined;
      try {
        await context.app.service("uploads").remove(benefit.picture, params);
      } catch (error) {
        logger.error(error);
      }
    }
  }
  return context;
};

const setUserData = async (context: HookContext): Promise<HookContext> => {
  const { field } = context.data;
  if (field) {
    const data: any = {};
    // data[field] = context.data.id;
    data[field] = context.data.storedFilePath;
    await context.app.service("users").patch(context.params.user._id, data);
  } else {
    const data = { picture: context.data.storedFilePath };
    // const data = { picture: context.data.id };
    await context.app
      .service("benefits")
      .patch(context.params.query.benefit, data);
  }
  return context;
};

const validateRequest = async (context: HookContext): Promise<HookContext> => {
  if (!context.params.query.type) {
    throw new BadRequest("missing query param: 'type");
  }
  if (
    !["document", "profile", "business", "benefit"].includes(
      context.params.query.type
    )
  ) {
    throw new BadRequest("invalid query param: 'type");
  }
  if (context.params.query.type === "benefit") {
    if (!context.params.query.benefit) {
      throw new BadRequest("missing query param: 'benefit");
    }
    try {
      context.params.query.benefit = mongoose.Types.ObjectId(
        context.params.query.benefit
      );
      const benefit: benefit = await context.app
        .service("benefits")
        .get(context.params.query.benefit);
      if (
        benefit.business_id.toHexString() !==
          context.params.user._id.toHexString() &&
        context.params.user.rol === roles.business
      ) {
        throw new Forbidden("you can't perform this operation");
      }
    } catch (error) {
      console.log(error);
      throw error;
      // throw new BadRequest("requested benefit doesn't exists");
    }
  }

  switch (context.params.query.type) {
    case "document":
      checkPermissions({
        roles: [roles.user, roles.userAdmin],
        field: "rol",
        error: true,
      })(context);
      break;
    case "profile":
      break;
    case "business":
      checkPermissions({
        roles: [roles.business, roles.businessAdmin],
        field: "rol",
        error: true,
      })(context);
      break;
    case "benefit":
      checkPermissions({
        roles: [roles.business, roles.businessAdmin],
        field: "rol",
        error: true,
      })(context);
      break;
    default:
      break;
  }
  return context;
};

const isUserUploadingDocument = (context: HookContext): boolean =>
  context.params.query.type === "document" &&
  context.params.user.rol === roles.user;

export default {
  before: {
    all: [authenticate("jwt")],
    find: [disallow()],
    get: [],
    create: [
      checkPermissions({
        roles: [
          roles.user,
          roles.business,
          roles.userAdmin,
          roles.businessAdmin,
        ],
        field: "rol",
        error: true,
      }),
      iff(
        isNot(isAdmin),
        iff(
          isNot(isUserUploadingDocument),
          verifyHooks.isVerified(),
          allowConfirmed
        )
      ),
      validateRequest,
      toDataURI,
      setFileName,
      setField,
      purgeOrphanFiles,
      setUserData,
    ],
    update: [disallow()],
    patch: [disallow()],
    remove: [
      iff(
        isProvider("external"),
        checkPermissions({
          roles: [roles.userAdmin, roles.businessAdmin, roles.admin],
          field: "rol",
          error: true,
        })
      ),
    ],
  },

  after: {
    all: [discard("uri")],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
