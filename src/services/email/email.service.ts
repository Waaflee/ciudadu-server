// Initializes the `email` service on path `/email`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations";
import Mailer from "feathers-mailer";
import nodemailer from "nodemailer";
import smtpTransport from "nodemailer-smtp-transport";
import hooks from "./email.hooks";

// Add this service to the service type index
declare module "../../declarations" {
  interface ServiceTypes {
    email: {
      create(data: any, prams?: any): Promise<any>;
    } & ServiceAddons<any>;
  }
}

interface smtpConfig {
  host: string;
  port: number;
  secure: boolean;
  proxy: string;
  auth: {
    user: string;
    pass: string;
  };
}
export default function (app: Application): void {
  // Initialize our service with any options it requires
  let smtpConfig: Partial<smtpConfig> = {};
  if (
    process.env.NODE_ENV === "development" ||
    process.env.NODE_ENV === "test"
  ) {
    smtpConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      secure: false,
      // proxy: process.env.http_proxy,
      auth: {
        user: "jerrod.metz28@ethereal.email",
        pass: "fGCY7DKfF55YKCpD77",
      },
    };
    app.use(
      "/email",
      Mailer(smtpTransport(smtpConfig), { from: app.get("from_email") })
    );
  } else {
    const transporter = nodemailer.createTransport({
      sendmail: true,
      newline: "unix",
      path: "/usr/sbin/sendmail",
    });
    app.use("/email", Mailer(transporter, { from: app.get("from_email") }));
  }
  // app.use(
  //   "/email",
  //   Mailer(smtpTransport(smtpConfig), { from: app.get("from_email") })
  // );
  // Get our initialized service so that we can register hooks
  const service = app.service("email");

  service.hooks(hooks);
}
