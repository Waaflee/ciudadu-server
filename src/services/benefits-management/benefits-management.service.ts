// Initializes the `benefits-management` service on path `/benefits-management`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { BenefitsManagement } from './benefits-management.class';
import createModel from '../../models/benefits-management.model';
import hooks from './benefits-management.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'benefits-management': BenefitsManagement & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/benefits-management', new BenefitsManagement(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('benefits-management');

  service.hooks(hooks);
}
