import * as authentication from "@feathersjs/authentication";
import { HookContext } from "@feathersjs/feathers";
import { debug, disallow } from "feathers-hooks-common";
import checkPermissions from "feathers-permissions";
import { roles } from "../../models/users.model";
import { benefit } from "../../models/benefits.model";
import { benefitNotification } from "../../models/benefits-management.model";
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

const populateNotification = async (
  context: HookContext,
  notification: benefitNotification
): Promise<benefitNotification> => {
  let benefit: any = {};
  try {
    benefit = await context.app.service("benefits").get(notification.benefitId);
    delete benefit.business.password;
  } catch (error) {
    benefit = { name: "beneficio borrado" };
  }
  notification.benefit = benefit;
  return notification;
};

const populateGet = async (context: HookContext): Promise<HookContext> => {
  context.result = await populateNotification(context, context.result);
  return context;
};

const populateFind = async (context: HookContext): Promise<HookContext> => {
  context.result = await Promise.all(
    (context.result as benefitNotification[]).map(async (notification) => {
      return await populateNotification(context, notification);
    })
  );
  return context;
};

// Should only be visible to business-admin role.
export default {
  before: {
    all: [authenticate("jwt")],
    find: [
      checkPermissions({
        roles: [roles.businessAdmin],
        field: "rol",
        error: true,
      }),
    ],
    get: [
      checkPermissions({
        roles: [roles.businessAdmin],
        field: "rol",
        error: true,
      }),
    ],
    create: [disallow("external")],
    update: [disallow("external")],
    patch: [disallow("external")],
    remove: [
      checkPermissions({
        roles: [roles.businessAdmin],
        field: "rol",
        error: true,
      }),
    ],
  },

  after: {
    all: [],
    find: [populateFind],
    get: [populateGet],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
