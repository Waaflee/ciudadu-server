// Initializes the `codes` service on path `/codes`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations";
import { Codes } from "./codes.class";
import createModel from "../../models/codes.model";
import hooks from "./codes.hooks";

// Add this service to the service type index
declare module "../../declarations" {
  interface ServiceTypes {
    codes: Codes & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
  };

  // Initialize our service with any options it requires
  app.use("/codes", new Codes(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("codes");

  service.hooks(hooks);
}
