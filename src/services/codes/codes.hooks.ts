// @ts-nocheck
import * as authentication from "@feathersjs/authentication";
import { setUserId, generateCodeId } from "../../hooks/code";
import { isAdmin, verifyEntity, verifyHooks } from "../../hooks";
import { disallow, iff, isNot } from "feathers-hooks-common";
import checkPermissions from "feathers-permissions";
import { roles, user } from "../../models/users.model";
import { allowConfirmed } from "../users/users.hooks";
import { HookContext } from "../../declarations";
import { benefit } from "../../models/benefits.model";
import { code } from "../../models/codes.model";
// Don't remove this comment. It's needed to format import lines nicely.
const { authenticate } = authentication.hooks;

const populateCode = async (
  context: HookContext,
  code: code
): Promise<code> => {
  try {
    const benefit: benefit = await context.app
      .service("benefits")
      .get(code.benefit);
    code.benefitData = benefit;
  } catch (error) {
    code.benefitData = { name: "El beneficio seleccionado ya no existe" };
  }
  const user: user = await context.app.service("users").get(code.user);
  delete user.password;
  code.userData = user;
  return code;
};

const populateGet = async (context: HookContext): Promise<HookContext> => {
  context.result = await populateCode(context, context.result);
  return context;
};

const populateFind = async (context: HookContext): Promise<HookContext> => {
  context.result = await Promise.all(
    (context.result as code[]).map(async (code) => {
      return await populateCode(context, code);
    })
  );
  return context;
};

export default {
  before: {
    all: [authenticate("jwt")],
    find: [
      checkPermissions({
        roles: [roles.user, roles.userAdmin, roles.business],
        field: "rol",
        error: true,
      }),
      iff(isNot(isAdmin), verifyHooks.isVerified(), allowConfirmed),
    ],
    get: [],
    create: [
      checkPermissions({
        roles: [roles.user],
        field: "rol",
        error: true,
      }),
      verifyHooks.isVerified(),
      allowConfirmed,
      verifyEntity({
        service: "benefits",
        field: "benefit",
        message: "Beneficio",
      }),
      setUserId,
      generateCodeId,
    ],
    update: [disallow()],
    patch: [disallow("external")],
    remove: [disallow("external")],
  },

  after: {
    all: [],
    find: [populateFind],
    get: [populateGet],
    create: [populateGet],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
