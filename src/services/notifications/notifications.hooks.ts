import * as authentication from "@feathersjs/authentication";
import { disallow, iff, isNot } from "feathers-hooks-common";
import checkPermissions from "feathers-permissions";
import { roles } from "../../models/users.model";
import {
  redisBeforeHook as redisBefore,
  redisAfterHook as redisAfter,
  hookCache as cache,
} from "feathers-hooks-rediscache";
import { isAdmin, verifyHooks } from "../../hooks";
import { allowConfirmed } from "../users/users.hooks";
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
  before: {
    all: [authenticate("jwt")],
    find: [
      redisBefore(),
      checkPermissions({
        roles: [roles.businessAdmin, roles.business],
        field: "rol",
        error: true,
      }),
      iff(isNot(isAdmin), verifyHooks.isVerified(), allowConfirmed),
    ],
    get: [disallow()],
    create: [
      checkPermissions({
        roles: [roles.businessAdmin],
        field: "rol",
        error: true,
      }),
    ],
    update: [disallow()],
    patch: [
      checkPermissions({
        roles: [roles.businessAdmin],
        field: "rol",
        error: true,
      }),
    ],
    remove: [
      checkPermissions({
        roles: [roles.businessAdmin],
        field: "rol",
        error: true,
      }),
    ],
  },

  after: {
    all: [],
    find: [cache({ duration: 3600 * 24 * 1 }), redisAfter()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
