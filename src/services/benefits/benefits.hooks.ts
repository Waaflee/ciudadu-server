import * as authentication from "@feathersjs/authentication";
import { Id } from "@feathersjs/feathers";
import { HookContext } from "../../declarations";
import { setField } from "feathers-authentication-hooks";
import {
  debug,
  disallow,
  iff,
  isNot,
  preventChanges,
} from "feathers-hooks-common";
import app from "../../app";
import { isAdmin, verifyHooks } from "../../hooks";
import { Forbidden } from "@feathersjs/errors";
import checkPermissions from "feathers-permissions";
import { roles, user } from "../../models/users.model";
import { allowConfirmed } from "../users/users.hooks";
import { User } from "feathers-authentication-management-ts";
import { benefit } from "../../models/benefits.model";
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

const addToBusiness = async (context: HookContext): Promise<HookContext> => {
  const business: User = await context.app
    .service("users")
    .get(context.params.user._id);
  await context.app.service("users").patch(context.params.user._id, {
    businessBenefits: [...business.businessBenefits, context.result._id],
  });

  return context;
};
const populateBenefit = async (
  context: HookContext,
  benefit: benefit
): Promise<benefit> => {
  const business: user = await context.app
    .service("users")
    .get(benefit.business_id);
  delete business.password;
  benefit.business = business;
  return benefit;
};

const populateGet = async (context: HookContext): Promise<HookContext> => {
  context.result = await populateBenefit(context, context.result);
  return context;
};

const populateFind = async (context: HookContext): Promise<HookContext> => {
  context.result = await Promise.all(
    (context.result as benefit[]).map(async (benefit) => {
      return await populateBenefit(context, benefit);
    })
  );
  return context;
};

const setBusinessId = setField({
  from: "params.user._id",
  as: "data.business_id",
});
const limitToBusiness = setField({
  from: "params.user._id",
  as: "params.query.business_id",
});

interface notificationData {
  benefitId: Id;
  operation: string;
  deletedData?: string;
}

const createNotification = async (
  context: HookContext
): Promise<HookContext> => {
  const data: notificationData = {
    benefitId: context.result?._id,
    operation: context.method,
  };
  if (context.method === "remove") {
    context.result.business = await app
      .service("users")
      .get(context.result.business_id);
    data.deletedData = JSON.stringify(context.result);
  }
  await app.service("benefits-management").create(data);
  return context;
};

const notChangingInterval = async (
  context: HookContext
): Promise<HookContext> => {
  const benefit = await app.service("benefits").get(context.id);
  const benefitLastChangeDate = new Date(benefit.updatedAt).getTime();
  const currentDate = new Date(Date.now()).getTime();
  const daysBetweenChanges = Math.floor(
    (currentDate - benefitLastChangeDate) / (1000 * 60 * 60 * 24)
  );
  if (daysBetweenChanges < app.get("benefitChangeFrequency")) {
    throw new Forbidden("You cannot perform this operation.", {
      error:
        "You must wait " +
        app.get("benefitChangeFrequency") +
        " days between changes.",
    });
  } else {
    return context;
  }
};

const isUpdatingPicture = (context: HookContext): boolean =>
  context.data.picture && context.params.file;

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authenticate("jwt"),
      checkPermissions({
        roles: [roles.business, roles.businessAdmin],
        field: "rol",
        error: true,
      }),
      iff(
        (context: HookContext) => context.params.user.rol === roles.business,
        verifyHooks.isVerified(),
        allowConfirmed,
        setBusinessId
      ),
    ],
    update: [disallow()],
    patch: [
      authenticate("jwt"),
      checkPermissions({
        roles: [roles.business, roles.businessAdmin],
        field: "rol",
        error: true,
      }),
      iff(
        isUpdatingPicture,
        preventChanges(
          true,
          "_id",
          "business_id",
          "name",
          "type",
          "category",
          "subcategory",
          "percentage",
          "acceptedPayments",
          "validDays",
          "uses",
          "validated"
        ),
        iff(
          isNot(isAdmin),
          verifyHooks.isVerified(),
          allowConfirmed,
          notChangingInterval,
          preventChanges(
            true,
            "_id",
            "business_id",
            "name",
            "type",
            "category",
            "subcategory",
            "percentage",
            // "acceptedPayments",
            // "validDays",

            "validated"
          ),
          limitToBusiness
        )
      ),
    ],
    remove: [authenticate("jwt"), iff(isNot(isAdmin), limitToBusiness)],
  },

  after: {
    all: [],
    find: [populateFind],
    get: [populateGet],
    create: [createNotification],
    // create: [addToBusiness, createNotification],
    update: [],
    patch: [createNotification],
    remove: [createNotification],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
