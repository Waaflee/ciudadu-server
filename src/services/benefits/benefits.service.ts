// Initializes the `benefits` service on path `/benefits`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Benefits } from './benefits.class';
import createModel from '../../models/benefits.model';
import hooks from './benefits.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'benefits': Benefits & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate'),
  };

  // Initialize our service with any options it requires
  app.use('/benefits', new Benefits(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('benefits');

  service.hooks(hooks);
}
