import { BadRequest, Forbidden } from "@feathersjs/errors";
import { Hook, HookContext } from "@feathersjs/feathers";
import { HookContext as DeclaredHookContext } from "../declarations";
import accountService from "../services/auth-management/notifier";
import authManagement from "feathers-authentication-management-ts";
export const verifyHooks = authManagement.hooks;

export const restrictToRol = (rol: string): Hook => {
  return (context: HookContext): HookContext => {
    if (context.params.user?.rol !== rol) {
      throw new Forbidden(
        `Must be an authenticated ${rol} account in order to do this.`
      );
    }
    return context;
  };
};

export const verifyEntity = (entity: {
  service: string;
  field: string;
  message: string;
}) => {
  return async (context: HookContext): Promise<HookContext> => {
    const { app, data, params } = context;
    try {
      await app.service(entity.service).get(data[entity.field], params);
    } catch (error) {
      throw new BadRequest(`El ${entity.message} seleccionado no existe.`);
    }
    return context;
  };
};

export const isRequestedRolAdmin = (context: HookContext): boolean => {
  return ["business-admin", "user-admin", "admin"].includes(context.data.rol);
};

export const isAdmin = (context: HookContext): boolean => {
  return ["business-admin", "user-admin", "admin"].includes(
    context.params.user.rol
  );
};

export const checkIfAdmin = (context: HookContext): HookContext => {
  if (!(context.params.user?.rol === "admin")) {
    throw new Forbidden("You cannot perform this operation.");
  } else {
    return context;
  }
};

export const sendVerificationEmail = (
  context: DeclaredHookContext
): HookContext => {
  if (!context.params.provider) {
    return context;
  }
  const user = context.result;
  if (context.data && context.data.email && user) {
    accountService(context.app).notifier("resendVerifySignup", user);
    return context;
  }
  return context;
};
