import { setField } from "feathers-authentication-hooks";
import app from "../app";
import { HookContext } from "../declarations";
import { RedeemsResponse } from "../services/redeems/redeems.class";

export const setUserId = setField({
  from: "params.user._id",
  as: "data.user",
});
export const limitToUser = setField({
  from: "params.user._id",
  as: "params.query.user",
});

export const verifyBenefit = async (
  context: HookContext
): Promise<HookContext> => {
  const { app, data, params } = context;
  try {
    await app.service("benefits").get(data.benefit, params);
  } catch (error) {
    throw new Error("El beneficio seleccionado no existe.");
  }
  return context;
};

export const countToId = (count: number): string => {
  const overflow = count % (36 * 36 * 36 * 36 - 1);
  return overflow.toString(36).padStart(4, "0");
};

export const generateCodeId = async (
  context: HookContext
): Promise<HookContext> => {
  const redeemsData = (await app
    .service("redeems")
    .create({})) as RedeemsResponse;
  context.data.code = countToId(redeemsData.globalCounter.created);
  return context;
};
