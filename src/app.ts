// import path from "path";
// import favicon from "serve-favicon";
import compress from "compression";
import helmet from "helmet";
import cors from "cors";

import feathers from "@feathersjs/feathers";
import configuration from "@feathersjs/configuration";
import express from "@feathersjs/express";

import { Application } from "./declarations";
import logger from "./logger";
import middleware from "./middleware";
import services from "./services";
import appHooks from "./app.hooks";
import channels from "./channels";
import { HookContext as FeathersHookContext } from "@feathersjs/feathers";
import authentication from "./authentication";
import mongoose from "./mongoose";

import rateLimiterRedisMiddleware from "./utils/ratelimiter";
import mongoSanitize from "express-mongo-sanitize";
import xss from "xss-clean";
import { cacheRoutes as routes, redisClient } from "feathers-hooks-rediscache";
import task from "./utils/cron.jobs";

// Don't remove this comment. It's needed to format import lines nicely.

const app: Application = express(feathers());
export type HookContext<T = any> = {
  app: Application;
} & FeathersHookContext<T>;

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(favicon(path.join(app.get("public"), "favicon.ico")));
// Host the public folder
// app.use(
//   "/",
//   express.static(app.get("private"), {
//     extensions: ["pdf"],
//   })
// );

// Set up Plugins and providers
app.configure(express.rest());

app.configure(mongoose);

// Configure other middleware (see `middleware/index.ts`)
app.configure(authentication);
app.configure(middleware);
// Set up our services (see `services/index.ts`)
app.configure(services);
// Set up event channels (see channels.ts)
app.configure(channels);

// Set up redis client
app.configure(redisClient);
// add the cache routes (endpoints) to the app
app.use("/cache", routes(app));
app.use(rateLimiterRedisMiddleware(app));
app.use(mongoSanitize());
// Data Sanitization against XSS
app.use(xss());

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);

task(app).start();

export default app;
