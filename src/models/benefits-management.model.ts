// benefits-management-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from "../declarations";
import { Model, Mongoose } from "mongoose";
import { Id } from "@feathersjs/feathers";
import { benefit } from "./benefits.model";

export default function (app: Application): Model<any> {
  const modelName = "benefitsManagement";
  const mongooseClient: Mongoose = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const schema = new Schema(
    {
      benefitId: {
        type: Schema.Types.ObjectId,
        ref: "benefits",
        required: true,
      },
      operation: {
        type: String,
        enum: ["create", "update", "patch", "remove"],
        required: true,
      },
      deletedData: {
        type: String,
        required: function () {
          this.operation === "remove";
        },
      },
      createdAt: { type: Date, expires: 2592000, default: Date.now },
    },
    {
      timestamps: true,
    }
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export interface benefitNotification {
  notification: import("/home/wafle/Documentos/Web/Ciudad U/server/src/models/users.model").user;
  _id?: Id;
  benefitId: Id;
  benefit: benefit;
  operation: "create" | "update" | "patch" | "remove";
  deletedData?: string;
}
