// benefits-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from "../declarations";
import { Model, Mongoose } from "mongoose";
import { Id } from "@feathersjs/feathers";
import { user } from "./users.model";

export default function (app: Application): Model<any> {
  const modelName = "benefits";
  const mongooseClient: Mongoose = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  // Type enum specifies the kind of benefit given it's initial.
  const schema = new Schema(
    {
      business_id: { type: Schema.Types.ObjectId, required: true },
      name: { type: String, required: true },
      type: { type: String, required: true },
      picture: { type: String, default: "/pictures/default.jpg" },
      category: { type: String, required: true },
      // subcategory: { type: String, required: true },
      percentage: { type: Number, min: 5, max: 100 },
      acceptedPayments: [{ type: String, required: true }],
      validDays: [{ type: String, required: true }],
      uses: { type: Number, min: 0, required: true, default: 0 },
      validated: {
        type: String,
        enum: ["yes", "pending", "denied"],
        default: "pending",
        required: true,
      },
      observations: { type: String, maxlength: 240 },
    },
    {
      timestamps: true,
    }
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export interface benefit {
  _id?: Id;
  business_id: any;
  business?: user;
  name: string;
  type: string;
  picture?: string;
  category?: string;
  // subcategory?: string;
  percentage?: number;
  acceptedPayments?: string[];
  validDays?: string[];
  uses: number;
  validated?: "yes" | "pending" | "denied";
}
