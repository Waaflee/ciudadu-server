// codes-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from "../declarations";
import { Model, Mongoose } from "mongoose";
import { Id } from "@feathersjs/feathers";
import { user } from "./users.model";
import { benefit } from "./benefits.model";

export default function (app: Application): Model<any> {
  const modelName = "codes";
  const mongooseClient: Mongoose = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const schema = new Schema(
    {
      code: {
        type: String,
        unique: true,
        required: true,
        default: Date.now().toString(36).substring(4),
      },
      used: { type: Boolean, default: false },
      benefit: {
        type: Schema.Types.ObjectId,
        ref: "benefits",
        required: true,
        // autopopulate: true,
      },
      user: {
        type: Schema.Types.ObjectId,
        ref: "users",
        required: true,
        // autopopulate: true,
      },
      createdAt: { type: Date, expires: 172800, default: Date.now },
    },
    {
      timestamps: true,
    }
  );
  // schema.plugin(require("mongoose-autopopulate"));

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export interface code {
  _id?: Id;
  code: string;
  used?: boolean;
  benefit: Id;
  benefitData?: benefit;
  user: Id;
  userData?: user;
}
