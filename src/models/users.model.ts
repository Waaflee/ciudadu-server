// users-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from "../declarations";
import { Model, Mongoose } from "mongoose";
import { Id } from "@feathersjs/feathers";

export default function (app: Application): Model<any> {
  const modelName = "users";
  const mongooseClient: Mongoose = app.get("mongooseClient");

  const schema = new mongooseClient.Schema(
    {
      email: { type: String, unique: true, lowercase: true, required: true },
      password: {
        type: String,
      },
      rol: {
        type: String,
        enum: ["user", "business", "business-admin", "user-admin", "admin"],
        required: true,
      },

      profilePicture: {
        type: String,
        default: "/pictures/default.jpg",
      },
      businessPicture: {
        type: String,
        default: "/pictures/default.jpg",
      },
      businessCategory: { type: String },
      businessName: { type: String },
      businessReview: { type: String },
      businessLink: { type: String },
      // businessBenefits: { type: Array },
      name: {
        type: String,
      },
      lastName: {
        type: String,
      },
      birthday: {
        type: String,
      },
      dni: {
        type: Number,
      },
      university: {
        type: String,
      },
      facultyAndCareer: {
        type: String,
      },
      registration: {
        type: Number,
      },
      sex: { type: String },
      phone: { type: String },
      address: { type: String },
      certificate: { type: String, default: "/private/default.pdf" },
      googleId: { type: String },
      isConfirmed: { type: Boolean, default: false, required: true },
      isVerified: { type: Boolean },
      verifyToken: { type: String },
      verifyExpires: { type: Date },
      verifyChanges: { type: Object },
      resetToken: { type: String },
      resetExpires: { type: Date },
    },
    {
      timestamps: true,
    }
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export interface user {
  _id?: Id;
  email: string;
  password: string;
  rol: "user" | "business" | "business-admin" | "user-admin" | "admin";
  profilePicture?: string;
  businessPicture?: string;
  businessCategory?: string;
  businessName?: string;
  businessReview?: string;
  businessLink?: string;
  // businessBenefits?: Id[];
  name?: string;
  lastName?: string;
  birthday?: string;
  dni?: number;
  university?: string;
  facultyAndCareer?: string;
  certificate?: string;
  registration?: number;
  sex?: string;
  phone?: string;
  address?: string;
  isConfirmed: boolean;
  googleId?: string;
  isVerified?: boolean;
  verifyToken?: string;
  verifyExpires?: Date;
  verifyChanges?: any;
  resetToken?: string;
  resetExpires?: Date;
}
export enum roles {
  admin = "admin",
  user = "user",
  userAdmin = "user-admin",
  business = "business",
  businessAdmin = "business-admin",
}
