// redeems-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from "../declarations";
import { Model, Mongoose } from "mongoose";
import { Id } from "@feathersjs/feathers";

export default function (app: Application): Model<any> {
  const modelName = "redeems";
  const mongooseClient: Mongoose = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const schema = new Schema(
    {
      type: {
        type: String,
        required: true,
        enum: ["global", "local"],
        default: "local",
      },
      simpleDate: {
        type: String,
        max: 7,
        min: 6,
        required: function () {
          return this.type === "local";
        },
        unique: true,
      },
      created: { type: Number, required: true },
      used: { type: Number, default: 0 },
      // used: { type: Number, required: true },
    },
    {
      timestamps: true,
    }
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export interface redeem {
  _id: Id;
  type: string;
  simpleDate?: string;
  created: number;
  used: number;
}
