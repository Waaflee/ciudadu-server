import { Forbidden, GeneralError } from "@feathersjs/errors";
import { Request, Response, NextFunction } from "express";
import { roles } from "../models/users.model";
import app from "../app";

export default () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return (req: Request, res: Response, next: NextFunction): void => {
    const { user } = req as any;

    if (user.rol === roles.userAdmin) {
      const filePath =
        app.get("private") + "/uploads/documents/" + req.params.file;

      res.download(filePath, function (err) {
        if (!err) {
          return; // file sent
        }
        if (err) {
          throw new GeneralError("something bad happened");
        }
      });
    } else {
      throw new Forbidden("Unauthorized");
    }
  };
};
