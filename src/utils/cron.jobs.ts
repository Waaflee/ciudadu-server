import { Application } from "../declarations";
import cron from "node-cron";
import { roles, user } from "../models/users.model";
import logger from "../logger";

interface usersResponse {
  data: any[];
}

const purgeUser = async function (user: user) {
  try {
    if (user.profilePicture !== "/pictures/default.jpg") {
      try {
        await this.service("uploads").remove(user.profilePicture);
      } catch (error) {
        logger.error("error while deleting user profilePicture");
      }
    }
    if (user.rol === roles.user) {
      if (user.certificate !== "/private/default.pdf") {
        try {
          await this.service("uploads").remove(user.certificate);
        } catch (error) {
          logger.error("error while deleting user certificate");
        }
      }
    }
    if (user.rol === roles.business) {
      if (user.businessPicture !== "/pictures/default.jpg")
        try {
          await this.service("uploads").remove(user.businessPicture);
        } catch (error) {
          logger.error("error while deleting business businessPicture");
        }
    }
    await this.service("users").remove(user._id);
    return 1;
  } catch (error) {
    logger.error("error while purging users");
  }
  return 0;
};

const task = (app: Application): cron.ScheduledTask => {
  return cron.schedule(
    "0 3 * * 0",
    async () => {
      const MAX_DAYS_UNVERIFIED =
        1000 * 60 * 60 * 24 * app.get("purgeUsersInterval");
      const unverifiedUsers = (await app.service("users").find({
        query: {
          $or: [{ rol: roles.user }, { rol: roles.business }],
          isVerified: false,
          createdAt: {
            $lt: new Date().getTime() - MAX_DAYS_UNVERIFIED,
          },
        },
      })) as usersResponse;
      unverifiedUsers.data.map(purgeUser, app);
      logger.info(`purged ${unverifiedUsers.data.length} users`);
    },
    { scheduled: false }
  );
};

export default task;
