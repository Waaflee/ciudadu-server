import redis from "redis";
import { RateLimiterRedis } from "rate-limiter-flexible";
import { Application } from "../declarations";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function (app: Application) {
  const redisClient = redis.createClient({
    ...app.get("redis"),
    enable_offline_queue: false,
  });

  const rateLimiter = new RateLimiterRedis({
    storeClient: redisClient,
    keyPrefix: "middleware",
    points: 10, // 10 requests
    duration: 1, // per 1 second by IP
  });
  const rateLimiterMiddleware = (
    req: any,
    res: any,
    next: () => void
  ): void => {
    rateLimiter
      .consume(req.ip)
      .then(() => {
        next();
      })
      .catch(() => {
        res.status(429).send("Too Many Requests");
      });
  };
  return rateLimiterMiddleware;
}
