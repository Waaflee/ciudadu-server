/* eslint-disable @typescript-eslint/ban-ts-comment */
import { ServiceAddons } from "@feathersjs/feathers";
import { AuthenticationService, JWTStrategy } from "@feathersjs/authentication";
import { LocalStrategy } from "@feathersjs/authentication-local";
import { expressOauth, OAuthStrategy } from "@feathersjs/authentication-oauth";

import { Application } from "./declarations";
import { roles } from "./models/users.model";

declare module "./declarations" {
  interface ServiceTypes {
    authentication: AuthenticationService & ServiceAddons<any>;
  }
}

class GoogleStrategy extends OAuthStrategy {
  async getEntityData(profile: any) {
    // this will set 'googleId'
    // @ts-ignore
    const baseData = await super.getEntityData(profile);

    // this will grab the picture and email address of the Google profile
    // only users can sign with google
    return {
      ...baseData,
      profilePicture: profile.picture,
      email: profile.email,
      name: profile.given_name,
      lastName: profile.family_name,
      rol: roles.user,
    };
  }
}

export default function (app: Application): void {
  const authentication = new AuthenticationService(app);

  authentication.register("jwt", new JWTStrategy());
  authentication.register("local", new LocalStrategy());
  authentication.register("google", new GoogleStrategy());

  app.use("/authentication", authentication);
  app.configure(expressOauth());
}
